# Zuul Demo: Bundled FE and BE

This project implements the following:

1. Generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.6.
2. Spring Boot V2 
3. RabbitMQ
4. Netflix ZUUL
5. Swagger UI
6. Swagger Codegen

## Developing

Most importantly, this application assumes the FTM-Scada->FTM-Services application running on port 82. Zuul will provide a proxy for accessing this API in the client Spring Boot Application.

To build environment:

1. Build docker environment:
	- `cd docker/zuul-demo-dev-env`
	- Start Docker containers: `./start.sh`

2. Build FE environment and deploy:
	- `mvn generate-resources`
	- `cd src/client`
	- Deploy NG content `./ng-deploy`

3. From the root of zuul-demo, run the following:
	- `mvn clean spring-boot:run`

## Running unit tests

Run `mvn clean verify` to execute the unit tests.

## Running FE end-to-end tests

Run `./ng e2e` from inside src/client to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


To View API info:

1. Build environment as stated above.
2. Access http://localhost:8080/swagger-ui.html


## TODO

1. Bundle FE deployment
2. Add Swagger FE Client
3. More realistic Environment
