package com.den.zuuldemo.filter;

import com.den.zuuldemo.config.RabbitConfig;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import java.io.IOException;

@Order(1)
public class ZuulFilter implements Filter {
    private final RabbitTemplate rabbitTemplate;
    private static final String FORMAT_REDIRECT_MSG = "Sending Users Redirect for url: %s";

    @Autowired
    public ZuulFilter(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filter)
                        throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            String url = ((HttpServletRequest) request).getRequestURI().toLowerCase();
            this.rabbitTemplate.convertAndSend(RabbitConfig.TOPIC_EXCHANGE, RabbitConfig.DEFAULT_TOPIC_KEY,
                                               String.format(FORMAT_REDIRECT_MSG, url));
        }
        filter.doFilter(request, response);
    }
}
