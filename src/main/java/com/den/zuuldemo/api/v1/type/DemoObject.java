package com.den.zuuldemo.api.v1.type;

public class DemoObject {
    private static final String FAKE_NAME = "FAKE_NAME %s";
    private Integer id;
    private String name;

    public DemoObject(Integer id) {
        this.id = id;
        this.setName(id);
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setName(Integer id) {
        this.setName(String.format(FAKE_NAME, String.valueOf(id)));
    }
}
