package com.den.zuuldemo.api.v1.controller;

import com.den.zuuldemo.api.v1.type.DemoObject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class DemoController {

    @GetMapping("/object/{id}")
    public DemoObject getObject(@PathVariable(value = "id") Integer id) {
        return new DemoObject(id);
    }
}
