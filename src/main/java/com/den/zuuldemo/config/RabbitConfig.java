package com.den.zuuldemo.config;

import com.den.zuuldemo.rabbit.Receiver;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class RabbitConfig {
    private static final String DEMO_QUEUE = "demo-queue";
    private static final String DEFAULT_METHOD = "receiveMessage";
    public static final String TOPIC_EXCHANGE = "demo-exchange";
    public static final String DEFAULT_TOPIC_KEY = "route.key.#";

    @Bean
    public Queue queue() {
        return new Queue(DEMO_QUEUE, false);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(TOPIC_EXCHANGE);
    }

    @Bean
    public Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(DEFAULT_TOPIC_KEY);
    }

    @Bean
    public MessageListenerAdapter messageListenerAdapter(Receiver receiver) {
        return new MessageListenerAdapter(receiver, DEFAULT_METHOD);
    }

    @Bean
    @Profile("!test")
    public SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                                    MessageListenerAdapter messageListenerAdapter) {
        return new SimpleMessageListenerContainer() {{
            setConnectionFactory(connectionFactory);
            setQueueNames(DEMO_QUEUE);
            setMessageListener(messageListenerAdapter);
        }};
    }
}
