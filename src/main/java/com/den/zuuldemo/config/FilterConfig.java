package com.den.zuuldemo.config;

import com.den.zuuldemo.filter.ZuulFilter;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {
    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public FilterConfig(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Bean
    public FilterRegistrationBean<ZuulFilter> zuulFilter() {
        FilterRegistrationBean<ZuulFilter> zuulBean = new FilterRegistrationBean<>();
        zuulBean.setFilter(new ZuulFilter(rabbitTemplate));
        zuulBean.addUrlPatterns("/users/*");

        return zuulBean;
    }
}
