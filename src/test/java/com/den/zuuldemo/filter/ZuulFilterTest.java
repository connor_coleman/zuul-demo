package com.den.zuuldemo.filter;

import com.den.zuuldemo.BaseTest;

import com.netflix.zuul.exception.ZuulException;

import org.mockito.Mockito;

import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@EnableZuulProxy
public class ZuulFilterTest extends BaseTest {

    @Test
    public void usersRedirectTest() {
        Mockito.doNothing().when(this.rabbitTemplate);
        try {
            this.mockMvc.perform(get("/users"));
        } catch (Exception ex) {
            //Zuul should throw Forwarding exception if it is working.
            assertTrue(ex instanceof ZuulException);
        }
    }
}
