package com.den.zuuldemo.api.v1.controller;

import com.den.zuuldemo.BaseTest;

import org.junit.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DemoControllerTest extends BaseTest {

    @Test
    public void getObjectTest() throws Exception {
        this.mockMvc.perform(get("/api/v1/object/12/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(12))
                .andExpect(jsonPath("$.name").value("FAKE_NAME 12"));
    }

    @Test
    public void getObjectBadRequestTest() throws Exception {
        this.mockMvc.perform(get("/api/v1/object/BadInteger/"))
                .andExpect(status().isBadRequest());
    }
}
